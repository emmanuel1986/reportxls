class ReportMailer < ApplicationMailer
    
    def send_email(user_email,attachment,filename,emailsubject,bcc_email,cc_email,
        emailbodyadditional1 = nil,emailbodyadditional2 = nil,emailbodyadditional3 = nil)
        @email_subject = emailsubject
		@email_bodyadditional1 = emailbodyadditional1
		@email_bodyadditional2 = emailbodyadditional2
		@email_bodyadditional3 = emailbodyadditional3
		attachments[filename] = File.read(attachment)
	    mail( 
	    	:to => user_email,
	    	:cc => cc_email,
	    	:bcc => bcc_email,
	    	:subject => emailsubject
	    )
    end
    
 end